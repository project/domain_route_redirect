About the Domain Route Redirect module
====

This module aims to help with the management of access and redirection of paths
and routes for different domains of a website, and builds up on the [Domain
Access](https://www.drupal.org/project/domain) module.

How it works
====

The module adds two new domain redirect configuration types that can be added
and administrated from the admin backend. The corresponding pages are accessible
from the main Domain Access administration pages under `/admin/config/domain` by
selecting the relevant operation from the drop-down menu of the domain entry.

Each of the two new redirect configuration types are added as children of an
existing domain configuration entry. The defined redirects will only trigger for
this specific domain that they are defined for. For each redirect it possible to
set the mode of redirection (as either permanent, temporary, or none), the
domain this redirection entry is meant to redirect to, as well as the paths or
routes that it triggers for.

The two new configuration types operate as follows:
1. Domain Path Redirects
   These will trigger very early in the request. At this stage, only the path,
   and not the route corresponding to this path is known.
2. Domain Route Redirects
   These will trigger slightly later in the request. This is when Drupal has
   already determined, which route the requested path corresponds to. It is
   possible to define both paths **and** routes for this redirect type. The
   redirect will trigger if an allowed path or route matches the respective path
   or route of the request.

The module **first** works through all Domain Path Redirects right at the start
of the requests. If it does not find any of them, it then **second** tries to
find a matching Domain Route Redirect. The order in which the redirect configs
are checked can be adjusted in the admin table of the corresponding configs. If
the module encounters a matching redirect entry with the redirection mode set to
'Do not redirect', it stops processing of all redirect configuration entries of
that type (but it may for example continue with the execution of a Route
Redirect even when it encountered a Path Redirect previously that
requested no redirection of that path).

Development status
====

This module is under active development. Comments and ideas regarding how this
module could be changed or re-designed are very much welcomed and should be done
via the issue system on the drupal.org project page.
