<?php

namespace Drupal\domain_route_redirect\EventSubscriber;

use Drupal\domain\Access\DomainAccessCheck;
use Drupal\domain\DomainNegotiatorInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Path\PathMatcherInterface;
use Drupal\Core\Routing\CurrentRouteMatch;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Sets the domain context for an http request.
 */
class DomainRedirectSubscriber implements EventSubscriberInterface {

  /**
   * The domain negotiator service.
   *
   * @var \Drupal\domain\DomainNegotiatorInterface
   */
  protected $domainNegotiator;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The Domain storage handler service.
   *
   * @var \Drupal\domain\DomainStorageInterface
   */
  protected $domainStorage;

  /**
   * The Domain storage handler service.
   *
   * @var \Drupal\domain_route_redirect\DomainPathRedirectStorageInterface
   */
  protected $domainPathRedirectStorage;

  /**
   * The Domain storage handler service.
   *
   * @var \Drupal\domain_route_redirect\DomainRouteRedirectStorageInterface
   */
  protected $domainRouteRedirectStorage;

  /**
   * The core access check service.
   *
   * @var \Drupal\Core\Access\AccessCheckInterface
   */
  protected $accessCheck;

  /**
   * The current user account.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $account;

  /**
   * The path matcher service.
   *
   * @var \Drupal\Core\Path\PathMatcherInterface
   */
  protected $pathMatcher;

  /**
   * The current route match.
   *
   * @var Drupal\Core\Routing\CurrentRouteMatch
   */
  protected $currentRouteMatch;

  /**
   * Constructs a DomainSubscriber object.
   *
   * @param \Drupal\domain\DomainNegotiatorInterface $negotiator
   *   The domain negotiator service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\domain\Access\DomainAccessCheck $access_check
   *   The access check interface.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The current user account.
   * @param \Drupal\Core\Path\PathMatcherInterface $path_matcher
   *   The path matcher service.
   * @param \Drupal\Core\Routing\CurrentRouteMatch $current_route_match
   *   The current route match.
   */
  public function __construct(DomainNegotiatorInterface $negotiator, EntityTypeManagerInterface $entity_type_manager, DomainAccessCheck $access_check, AccountInterface $account, PathMatcherInterface $path_matcher, CurrentRouteMatch $current_route_match) {
    $this->domainNegotiator = $negotiator;
    $this->entityTypeManager = $entity_type_manager;
    $this->domainStorage = $this->entityTypeManager->getStorage('domain');
    $this->domainPathRedirectStorage = $this->entityTypeManager->getStorage('domain_path_redirect');
    $this->domainRouteRedirectStorage = $this->entityTypeManager->getStorage('domain_route_redirect');
    $this->accessCheck = $access_check;
    $this->account = $account;
    $this->pathMatcher = $path_matcher;
    $this->currentRouteMatch = $current_route_match;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    /*  There are two events we subscribe to. The first is very early in the
     *  kernel request to forward based on the request path. The second is
     *  later in the call after the route corresponding to the path was found.
     */
    $events[KernelEvents::REQUEST][] = ['onKernelRequestDomain', 50];
    $events[KernelEvents::CONTROLLER][] = ['onKernelRouteDeterminedDomain', -50];
    return $events;
  }

  /**
   * Decides whether to redirect depending on the identified path.
   *
   * @param \Symfony\Component\HttpKernel\Event\GetResponseEvent $event
   *   The Event to process.
   */
  public function onKernelRequestDomain(GetResponseEvent $event) {
    // Negotiate the request and set domain context.
    if ($active_domain = $this->domainNegotiator->getActiveDomain(TRUE)) {
      /** @var \Drupal\domain\DomainInterface $active_domain */
      $domain_path_redirects = $this->domainPathRedirectStorage->loadByDomain($active_domain);
      $current_path = $event->getRequest()->getRequestUri();

      foreach ($domain_path_redirects as $domain_path_redirect) {
        $allowed_paths = $domain_path_redirect->getRedirectPaths();
        if ($this->checkPathMatch($current_path, $allowed_paths)) {
          $domain_url = $domain_path_redirect->getDomainTarget()->getUrl();
          $redirect_code = $domain_path_redirect->getRedirect();

          if ($redirect_code) {
            $response = new TrustedRedirectResponse($domain_url, $redirect_code);
            $event->setResponse($response);
          }

          break;
        }
      }
    }
  }

  /**
   * Checks whether a path matches the paths to be redirected.
   */
  private function checkPathMatch(string $path, $allowed_paths) {
    return $this->pathMatcher->matchPath($path, $allowed_paths);
  }

  /**
   * Decides whether to redirect depending on the identified route.
   *
   * @param \Symfony\Component\HttpKernel\Event\FilterControllerEvent $event
   *   The Event to process.
   */
  public function onKernelRouteDeterminedDomain(FilterControllerEvent $event) {
    if (!$event->isMasterRequest()) {
      return;
    }
    // Negotiate the request and set domain context.
    if ($active_domain = $this->domainNegotiator->getActiveDomain(TRUE)) {
      /** @var \Drupal\domain\DomainInterface $active_domain */
      $domain_route_redirects = $this->domainRouteRedirectStorage->loadByDomain($active_domain);
      $current_path = $event->getRequest()->getRequestUri();
      $current_route = $this->currentRouteMatch->getRouteName();

      foreach ($domain_route_redirects as $domain_route_redirect) {
        $allowed_paths = $domain_route_redirect->getRedirectPaths();
        $allowed_routes = $domain_route_redirect->getRedirectRoutes();

        if ($this->checkPathMatch($current_path, $allowed_paths)
        || $this->checkRouteMatch($current_route, $allowed_routes)) {
          $domain_url = $domain_route_redirect->getDomainTarget()->getUrl();
          $redirect_code = $domain_route_redirect->getRedirect();

          if ($redirect_code) {
            $event->stopPropagation();
            $event->setController(function () use ($domain_url, $redirect_code) {
                return new TrustedRedirectResponse($domain_url, $redirect_code);
            });
          }

          break;
        }
      }
    }
  }

  /**
   * Checks whether a route matches the routes to be redirected.
   */
  private function checkRouteMatch(string $route, $allowed_routes) {
    return in_array($route, $allowed_routes);
  }

}
