<?php

namespace Drupal\domain_route_redirect\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\domain\DomainInterface;

/**
 * Returns responses for Domain Redirect module routes.
 */
class DomainRouteRedirectController extends ControllerBase {

  /**
   * Provides the domain route redirect submission form.
   *
   * @param \Drupal\domain\DomainInterface $domain
   *   An domain record entity.
   *
   * @return array
   *   Returns the domain route redirect submission form.
   */
  public function addRouteRedirect(DomainInterface $domain) {
    // The entire purpose of this controller is to add the values from
    // the parent domain entity.
    $values['domain_id'] = $domain->id();

    // Create the stub route redirect with reference to the parent domain.
    $redirect = $this->entityTypeManager()->getStorage('domain_route_redirect')->create($values);

    return $this->entityFormBuilder()->getForm($redirect);
  }

  /**
   * Provides the listing page for route redirects.
   *
   * @param \Drupal\domain\DomainInterface $domain
   *   An domain record entity.
   *
   * @return array
   *   A render array as expected by drupal_render().
   */
  public function listing(DomainInterface $domain) {
    $list = $this->entityTypeManager()->getListBuilder('domain_route_redirect');
    $list->setDomain($domain);
    return $list->render();
  }

}
