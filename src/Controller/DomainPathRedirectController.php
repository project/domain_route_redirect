<?php

namespace Drupal\domain_route_redirect\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\domain\DomainInterface;

/**
 * Returns responses for Domain Redirect module routes.
 */
class DomainPathRedirectController extends ControllerBase {

  /**
   * Provides the domain path redirect submission form.
   *
   * @param \Drupal\domain\DomainInterface $domain
   *   An domain record entity.
   *
   * @return array
   *   Returns the domain path redirect submission form.
   */
  public function addPathRedirect(DomainInterface $domain) {
    // The entire purpose of this controller is to add the values from
    // the parent domain entity.
    $values['domain_id'] = $domain->id();

    // Create the stub path redirect with reference to the parent domain.
    $redirect = $this->entityTypeManager()->getStorage('domain_path_redirect')->create($values);

    return $this->entityFormBuilder()->getForm($redirect);
  }

  /**
   * Provides the listing page for path redirects.
   *
   * @param \Drupal\domain\DomainInterface $domain
   *   An domain record entity.
   *
   * @return array
   *   A render array as expected by drupal_render().
   */
  public function listing(DomainInterface $domain) {
    $list = $this->entityTypeManager()->getListBuilder('domain_path_redirect');
    $list->setDomain($domain);
    return $list->render();
  }

}
