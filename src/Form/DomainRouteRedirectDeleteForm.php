<?php

namespace Drupal\domain_route_redirect\Form;

use Drupal\Core\Entity\EntityDeleteForm;
use Drupal\Core\Url;

/**
 * Builds the form to delete a route redirect record.
 */
class DomainRouteRedirectDeleteForm extends EntityDeleteForm {

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('domain_route_redirect.admin', [
      'domain' => $this->entity->getDomainId(),
    ]);
  }

}
