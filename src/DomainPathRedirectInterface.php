<?php

namespace Drupal\domain_route_redirect;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining a domain path redirect entity.
 */
interface DomainPathRedirectInterface extends ConfigEntityInterface {

  /**
   * Get the name for a path redirect record.
   *
   * @return string
   *   The name of the redirect.
   */
  public function getName();

  /**
   * Get the numeric domain_id value for a path redirect record.
   *
   * @return string
   *   The domain id for the path redirect record.
   */
  public function getDomainId();

  /**
   * Get the parent domain entity for a path redirect record.
   *
   * @return \Drupal\domain\Entity\Domain
   *   The parent domain for the path redirect record or NULL if not set.
   */
  public function getDomain();

  /**
   * Get the redirect value (301|302|NULL) for a path redirect record.
   *
   * @return int
   *   The redirect value.
   */
  public function getRedirect();

  /**
   * Get the redirect paths for a path redirect record.
   *
   * @return string
   *   The paths.
   */
  public function getRedirectPaths();

  /**
   * Get the domain target id.
   *
   * @return int
   *   The domain target id to which to redirect.
   */
  public function getDomainTargetId();

  /**
   * Get the domain target.
   *
   * @return \Drupal\domain\Entity\Domain
   *   The domain target to which to redirect.
   */
  public function getDomainTarget();

  /**
   * Returns the weight of the redirect.
   *
   * @return int
   *   The weight of the redirect.
   */
  public function getWeight();

  /**
   * Sets the weight of the redirect.
   *
   * @param int $weight
   *   The weight, used to order redirect with larger positive weights sinking
   *   items toward the bottom of lists.
   *
   * @return $this
   */
  public function setWeight($weight);

}
