<?php

namespace Drupal\domain_route_redirect;

use Drupal\Core\Config\Entity\ConfigEntityStorageInterface;
use Drupal\domain\DomainInterface;

/**
 * Supplies storage methods for common domain_path_redirect requests.
 */
interface DomainPathRedirectStorageInterface extends ConfigEntityStorageInterface {

  /**
   * Gets the schema for domain path redirect records.
   *
   * @return array
   *   An array representing the field schema of the object.
   */
  public function loadSchema();

  /**
   * Loads a domain path redirect record by parent domain lookup.
   *
   * @param \Drupal\domain\DomainInterface $domain
   *   A domain entity.
   *
   * @return array
   *   An array of \Drupal\domain_route_redirect\DomainPathRedirectInterface objects.
   */
  public function loadByDomain(DomainInterface $domain);

}
