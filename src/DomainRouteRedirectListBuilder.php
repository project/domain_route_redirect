<?php

namespace Drupal\domain_route_redirect;

use Drupal\domain\DomainInterface;
use Drupal\Core\Config\Entity\DraggableListBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * User interface for the domain route redirect overview screen.
 */
class DomainRouteRedirectListBuilder extends DraggableListBuilder {

  /**
   * A domain object loaded from the controller.
   *
   * @var \Drupal\domain\DomainInterface
   */
  protected $domain;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'domain_admin_route_redirect_overview_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header = [];

    $header['label'] = $this->t('Name');
    $header['redirect'] = $this->t('Redirect');
    $header['path_patterns'] = $this->t('Path patterns');
    $header['route_patterns'] = $this->t('Routes');
    $header['domain_target'] = $this->t('Target domain');

    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row = [];

    $row['label'] = $entity->label();
    $redirect = $entity->getRedirect();
    $row['redirect'] = ['#markup' => empty($redirect) ? $this->t('None') : $redirect];
    $row['path_patterns'] = ['#markup' => $entity->getRedirectPaths()];
    $row['route_patterns'] = ['#theme' => 'item_list', '#items' => $entity->getRedirectRoutes()];
    $row['domain_target'] = ['#markup' => $entity->getDomainTarget()->label()];

    return $row + parent::buildRow($entity);
  }

  /**
   * Loads entity IDs using a pager sorted by the entity id.
   *
   * @return array
   *   An array of entity IDs.
   */
  protected function getEntityIds() {
    $query = $this->getStorage()->getQuery()
      ->condition('domain_id', $this->getDomainId())
      ->sort($this->entityType->getKey('weight'));

    // Only add the pager if a limit is specified.
    if ($this->limit) {
      $query->pager($this->limit);
    }
    return $query->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    $form['actions']['submit']['#value'] = $this->t('Save configuration');
    return $form;
  }

  /**
   * Sets the domain context for this list.
   *
   * @param \Drupal\domain\DomainInterface $domain
   *   The domain to set as context for the list.
   */
  public function setDomain(DomainInterface $domain) {
    $this->domain = $domain;
  }

  /**
   * Gets the domain context for this list.
   *
   * @return \Drupal\domain\DomainInterface
   *   The domain that is context for this list.
   */
  public function getDomainId() {
    // @TODO: check for a use-case where we might need to derive the id?
    return !empty($this->domain) ? $this->domain->id() : NULL;
  }

}
