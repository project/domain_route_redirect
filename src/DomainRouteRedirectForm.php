<?php

namespace Drupal\domain_route_redirect;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\domain\DomainStorageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Messenger\Messenger;

/**
 * Base form controller for domain route redirect edit forms.
 */
class DomainRouteRedirectForm extends EntityForm {

  /**
   * The configuration factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $config;

  /**
   * The domain entity access control handler.
   *
   * @var \Drupal\domain\DomainAccessControlHandler
   */
  protected $accessHandler;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The domain route redirect storage manager.
   *
   * @var \Drupal\domain_route_redirect\DomainRouteRedirectStorageInterface
   */
  protected $redirectStorage;

  /**
   * The domain storage manager.
   *
   * @var \Drupal\domain\DomainStorageInterface
   */
  protected $domainStorage;

  /**
   * Drupal\Core\Logger\LoggerChannelInterface definition.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $loggerChannelDomainRedirect;

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\Messenger
   */
  protected $messenger;

  /**
   * Constructs a DomainRouteRedirectForm object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config
   *   The configuration factory service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\domain_route_redirect\DomainRouteRedirectStorageInterface $redirectStorage
   *   The route redirect storage.
   * @param \Drupal\domain\DomainStorageInterface $domain_storage
   *   The domain storage manager.
   * @param \Drupal\Core\Logger\LoggerChannelInterface $logger_channel
   *   The logger channel.
   * @param \Drupal\Core\Messenger\Messenger $messenger
   *   The messenger.
   */
  public function __construct(ConfigFactoryInterface $config, EntityTypeManagerInterface $entity_type_manager, DomainRouteRedirectStorageInterface $redirectStorage, DomainStorageInterface $domain_storage, LoggerChannelInterface $logger_channel, Messenger $messenger) {
    $this->config = $config;
    $this->entityTypeManager = $entity_type_manager;
    $this->redirectStorage = $redirectStorage;
    $this->domainStorage = $domain_storage;
    // Not loaded directly since it is not an interface.
    $this->accessHandler = $this->entityTypeManager->getAccessControlHandler('domain');
    $this->loggerChannel = $logger_channel;
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager'),
      $container->get('entity_type.manager')->getStorage('domain_route_redirect'),
      $container->get('entity_type.manager')->getStorage('domain'),
      $container->get('logger.channel.domain_route_redirect'),
      $container->get('messenger')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\domain_route_redirect\DomainRouteRedirectInterface $redirect */
    $redirect = $this->entity;

    $form['domain_id'] = [
      '#type' => 'value',
      '#value' => $redirect->getDomainId(),
    ];
    $form['name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Administrative name'),
      '#size' => 40,
      '#maxlength' => 80,
      '#default_value' => $redirect->getName(),
      '#description' => $this->t('The name displayed the admin interface.'),
      '#required' => TRUE,
    ];
    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $redirect->id(),
      '#machine_name' => [
        'source' => ['name'],
        'exists' => [$this, 'exist'],
      ],
    ];
    $form['redirect'] = [
      '#type' => 'select',
      '#title' => $this->t('Redirection mode'),
      '#options' => $this->redirectOptions(),
      '#default_value' => $redirect->getRedirect(),
      '#description' => $this->t('Set an optional redirect directive when this route redirect is invoked.'),
    ];
    $form['path_patterns'] = [
      '#type' => 'textarea',
      '#rows' => 5,
      '#columns' => 40,
      '#title' => $this->t('Paths that should be forwarded'),
      '#default_value' => $redirect->getRedirectPaths(),
      '#description' => $this->t('Enter a list of paths to be forwarded to the hostname. One path pattern per line.'),
    ];
    $form['route_patterns'] = [
      '#type' => 'textarea',
      '#rows' => 5,
      '#columns' => 40,
      '#title' => $this->t('Routes that should be forwarded'),
      '#default_value' => $redirect->getRedirectRoutesRaw(),
      '#description' => $this->t('Enter a comma-separated list of routes to be forwarded to the hostname. Spaces and linebreaks are ignored.'),
    ];
    $options = [];
    foreach ($this->domainStorage->loadMultiple() as $domain) {
      $options[$domain->id()] = $domain->getHostname();
    }
    $form['domain_target'] = [
      '#type' => 'select',
      '#title' => $this->t('Target domain'),
      '#default_value' => $redirect->getDomainTargetId(),
      '#description' => $this->t("The target domain to redirect to. You have to select this even when you set the redirection mode to 'Do not redirect'."),
      '#options' => $options,
      '#required' => TRUE,

      /* unfortunately, there's a bug with the states API, meaning that the
       * 'optional' state is ignored for some reason. it may be possible to
       * uncomment these lines in the future, but for the moment the bug would
       * result in the form being broken. this is probably the related bug:
       * https://www.drupal.org/project/drupal/issues/2855139
       */

      // '#states' => [
      // 'optional' => [':input[name="redirect"]' => ['value' => 0],],
      // 'invisible' => [':input[name="redirect"]' => ['value' => 0],],
      // ],.
    ];

    return parent::form($form, $form_state);
  }

  /**
   * Returns a list of valid redirect options for the form.
   *
   * @return array
   *   A list of valid redirect options.
   */
  public function redirectOptions() {
    return [
      0 => $this->t('Do not redirect'),
      301 => $this->t('301 redirect: Moved Permanently'),
      302 => $this->t('302 redirect: Found'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\domain_route_redirect\DomainRouteRedirectInterface $redirect */
    $redirect = $this->entity;
    $edit_link = $redirect->toLink($this->t('Edit'), 'edit-form')->toString();
    if ($redirect->save() == SAVED_NEW) {
      $this->messenger->addMessage($this->t('Created new domain route redirect.'));
      $this->loggerChannel->notice('Created new domain route redirect %name.', ['%name' => $redirect->label(), 'link' => $edit_link]);
    }
    else {
      $this->messenger->addMessage($this->t('Updated domain route redirect.'));
      $this->loggerChannel->notice('Updated domain route redirect %name.', ['%name' => $redirect->label(), 'link' => $edit_link]);
    }
    $form_state->setRedirect('domain_route_redirect.admin', ['domain' => $redirect->getDomainId()]);
  }

  /**
   * Helper function to check whether a domain_route_redirect entity exists.
   */
  public function exist($id) {
    $entity = $this->entityTypeManager->getStorage('domain_route_redirect')->getQuery()
      ->condition('id', $id)
      ->execute();
    return (bool) $entity;
  }

}
