<?php

namespace Drupal\domain_route_redirect\Entity;

use Drupal\domain_route_redirect\DomainPathRedirectInterface;
use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Entity\EntityStorageInterface;

/**
 * Defines the domain path redirect entity.
 *
 * @ConfigEntityType(
 *   id = "domain_path_redirect",
 *   label = @Translation("Domain path redirect"),
 *   module = "domain_route_redirect",
 *   handlers = {
 *     "storage" = "Drupal\domain_route_redirect\DomainPathRedirectStorage",
 *     "access" = "Drupal\domain_route_redirect\DomainPathRedirectAccessControlHandler",
 *     "list_builder" = "Drupal\domain_route_redirect\DomainPathRedirectListBuilder",
 *     "form" = {
 *       "default" = "Drupal\domain_route_redirect\DomainPathRedirectForm",
 *       "edit" = "Drupal\domain_route_redirect\DomainPathRedirectForm",
 *       "delete" = "Drupal\domain_route_redirect\Form\DomainPathRedirectDeleteForm"
 *     }
 *   },
 *   config_prefix = "path_redirect",
 *   admin_permission = "administer domain path redirects",
 *   entity_keys = {
 *     "id" = "id",
 *     "domain_id" = "domain_id",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "weight" = "weight",
 *   },
 *   links = {
 *     "delete-form" = "/admin/config/domain/path_redirect/delete/{domain_path_redirect}",
 *     "edit-form" = "/admin/config/domain/path_redirect/edit/{domain_path_redirect}",
 *   },
 *   config_export = {
 *     "id",
 *     "domain_id",
 *     "name",
 *     "redirect",
 *     "path_patterns",
 *     "domain_target",
 *     "weight",
 *   }
 * )
 */
class DomainPathRedirect extends ConfigEntityBase implements DomainPathRedirectInterface {

  /**
   * The ID of the domain path redirect entity.
   *
   * @var string
   */
  protected $id;

  /**
   * The parent domain record ID.
   *
   * @var string
   */
  protected $domain_id;

  /**
   * The domain path redirect record UUID.
   *
   * @var string
   */
  protected $uuid;

  /**
   * The domain path redirect label.
   *
   * @var string
   */
  protected $name;

  /**
   * The weight of the domain path redirect entry.
   *
   * @var int
   */
  protected $weight = 0;

  /**
   * The domain path redirect record redirect value.
   *
   * @var int
   */
  protected $redirect;

  /**
   * The domain path redirect path patterns.
   *
   * @var string
   */
  protected $path_patterns;

  /**
   * The domain path redirect target domain.
   *
   * @var string
   */
  protected $domain_target;

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->name;
  }

  /**
   * {@inheritdoc}
   */
  public function getDomainId() {
    return $this->domain_id;
  }

  /**
   * {@inheritdoc}
   */
  public function getDomain() {
    $storage = \Drupal::entityTypeManager()->getStorage('domain');
    $domains = $storage->loadByProperties(['domain_id' => $this->domain_id]);
    return $domains ? current($domains) : NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getRedirect() {
    return $this->redirect;
  }

  /**
   * {@inheritdoc}
   */
  public function getRedirectPaths() {
    return $this->path_patterns;
  }

  /**
   * {@inheritdoc}
   */
  public function getDomainTargetId() {
    return $this->domain_target;
  }

  /**
   * {@inheritdoc}
   */
  public function getDomainTarget() {
    $storage = \Drupal::entityTypeManager()->getStorage('domain');
    $domains = $storage->loadByProperties(['id' => $this->domain_target]);
    return $domains ? current($domains) : NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getWeight() {
    return $this->weight;
  }

  /**
   * {@inheritdoc}
   */
  public function setWeight($weight) {
    $this->weight = $weight;
  }

  /**
   * {@inheritdoc}
   */
  public function postSave(EntityStorageInterface $storage, $update = TRUE) {
    parent::postSave($storage, $update);
    // Invalidate cache tags relevant to domains.
    \Drupal::service('cache_tags.invalidator')->invalidateTags(['rendered', 'url.site']);
  }

}
